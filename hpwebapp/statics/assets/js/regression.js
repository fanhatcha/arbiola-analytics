
  function regdatasetchange(id)
  {

      $.ajax({
          type: 'POST',
          data: {'id': id},
          beforeSend:function(xhr, settings) {
                  function getCookie(name) {
                          var cookieValue = null;
                          if (document.cookie && document.cookie != '') {
                              var cookies = document.cookie.split(';');
                              for (var i = 0; i < cookies.length; i++) {
                                  var cookie = jQuery.trim(cookies[i]);
                                  if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                      cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                      break;
                                  }
                              }
                          }
                          return cookieValue;
                      }
                      if (settings.url == "/browse-dataset/")
                          xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                  },
          url: '/browse-dataset/',
          dataType: 'json',
          success: function(res){
             var str=""
             var dataset_name = res[1]

             for (var i=0; i<res[0].length; i++) {
               str += "<option>" + res[0][i] + "</option>"
             }
             document.getElementById("pickonereg").innerHTML = str;
             document.getElementById("dataset-namereg").value = dataset_name;
             var selectModel = document.getElementById("model-slct");

             for(var i=0; i<res[2].length; i++)
                  {
                     var opt = document.createElement("option");
                     opt.value= res[2][i];
                     opt.innerHTML = res[2][i];
                     selectModel.appendChild(opt);
                  }

              var s2 = document.getElementById("slct2reg");
              s2.innerHTML = "";
             /***********************************************************************/

             for (var i=0; i<res[0].length; i++ ) {
                    var pair = res[0][i];
                    var checkbox = document.createElement("input");
                    checkbox.type = "checkbox";
                    checkbox.name = "independentVariablesReg";
                    checkbox.value = pair;
                    s2.appendChild(checkbox);

                    var label = document.createElement('label')
                    label.htmlFor = pair;
                    label.appendChild(document.createTextNode(pair));

                    s2.appendChild(label);
                    s2.appendChild(document.createElement("br"));
             }
             /***********************************************************************/
          },
          error: function(res){
              $('#message').text('Error!');
              $('.dvLoading').hide();
          }
      });
  }
