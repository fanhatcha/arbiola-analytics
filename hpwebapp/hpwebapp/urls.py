from django.contrib import admin
from django.urls import path
from arbiola import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name='home'),
    path('try-backend/', views.train_model, name='train_model'),
    path('upload-file/', views.upload_file, name='upload_file'),
    path('supervised-learning/', views.supervised_learning_page, name='supervised_learning_page'),
    # path('browse-dataset/<int:id>', views.browse_dataset, name='browse_dataset')
    path('browse-dataset/', views.browse_dataset, name='browse_dataset'),
    path('train-classification-model/', views.train_classification_model, name='train_classification_model'),
    path('analysis/', views.analysis_page, name='analysis_page'),
    path('make-a-new-prediction/', views.make_new_prediction, name='make_new_prediction'),
    path('unsupervised-learning/', views.unsupervised_learning_page, name='unsupervised_learning_page'),
    path('train-clustering-model/', views.train_clustering_model, name='train_clustering_model'),
    path('models', views.models_page, name='models_page'),
    path('open-model/<int:id>', views.open_model, name='open_model'),
    path('train-regression-model', views.train_regression_model, name='train_regression_model'),
    path('browse-clf-dataset/', views.browse_clf_dataset, name='browse_clf_dataset'),
    path('analysis-chart/', views.analysis_chart, name='analysis-chart'),

    path('register/', views.register_page, name="register"),
	path('login/', views.login_page, name="login"),
	path('logout/', views.logout_user, name="logout"),

    path('user-guide', views.user_guide_page, name='user_guide_page'),
]
