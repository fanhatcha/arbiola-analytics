from django.shortcuts import render, redirect
from django.http import HttpResponse

from arbiola.ml_templates import logistic_regression_API, multiple_linear_regression_API, Nystroem_API
from arbiola.ml_templates import RBF_Sampler_API, SGD_Classifier_API, SGD_Regressor_API, simple_linear_regression_API

import pandas as pd
from django.core.files.storage import FileSystemStorage

from django.http import JsonResponse
from arbiola.helpers import process_csv
from arbiola.helpers import predictor_API
import json

from arbiola.models import TrainedModels
from arbiola.models import UserDatasets
from django.db.models import Sum

import uuid
from django.contrib import messages
import requests

from django.utils.html import strip_tags


from django.forms import inlineformset_factory
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

# Create your views here.
from .models import *
from .forms import CreateUserForm

CSV_FILE_BASE_PATH="C:\\Users\\23058\\Documents\\GCU\\Honours Project\\artifact\\hpwebapp\\uploads\\"

@login_required(login_url='login')
def home(request):
        current_user = request.user
        list_of_datasets = UserDatasets.objects.filter(user_id=current_user.id).order_by('id')[::-1]
        return render(request, 'home.html',{
        'list_of_datasets':list_of_datasets,
        'username': current_user.username
        })

@login_required(login_url='login')
def train_model(request):
    print('--------------------------------------')
    passed_data = linear_regression.trainer()
    print(passed_data)
    print('--------------------------------------')
    return HttpResponse('request made ...')

@login_required(login_url='login')
def upload_file(request):
    if request.method == 'POST' and request.FILES['csv_file']:

        template_name = 'home.html'

       # Fetching and storing file uploaded in this variable
        csvfile = request.FILES['csv_file']

       # Storing dataset name entered by user in this variable
        raw_dataset_name = request.POST.get('dataset-name')

        # if file uploaded is a .CSV file then store file in permanent storage and save reference in db
        if csvfile.name.endswith('.csv'):
            # Generate a random unique name for the uploaded datasets
            ext = csvfile.name.split('.')[-1]
            final_name = "%s.%s" % (uuid.uuid4(), ext)

            # Store file in permanent file storage
            fs = FileSystemStorage()
            filename = fs.save(final_name, csvfile)
            uploaded_file_url = fs.url(filename)

            # Current logged in user
            current_user = request.user

            # Save file's references in database
            save_dataset = UserDatasets.objects.create(dataset_name=strip_tags(raw_dataset_name), saved_dataset_name=final_name, user_id=current_user.id)
            save_dataset.save()

            # Return all the datasets stored in db according to the authenticated user
            list_of_datasets = UserDatasets.objects.filter(user_id=1).order_by('id')[::-1]

            return render(request, 'home.html',{
                    'list_of_datasets':list_of_datasets,
                    'username':current_user.username
                    })
        else:
            # Return all the datasets stored in db according to the authenticated user
            list_of_datasets = UserDatasets.objects.filter(user_id=1).order_by('id')[::-1]

            return render(request, template_name,{
            'list_of_datasets':list_of_datasets,
            'error_msg': "Dataset must be a .CSV file !"
            })
    return render(request, template_name)


@login_required(login_url='login')
def supervised_learning_page(request):
    # Current logged in user
    current_user = request.user
    list_of_datasets = UserDatasets.objects.filter(user_id=current_user.id).order_by('id')[::-1]
    return render(request, 'supervised-learning.html',{
            'list_of_datasets':list_of_datasets,
            'username': current_user.username
            })
@login_required(login_url='login')
def unsupervised_learning_page(request):
    # Current logged in user
    current_user = request.user
    list_of_datasets = UserDatasets.objects.filter(user_id=current_user.id).order_by('id')[::-1]
    return render(request, 'unsupervised-learning.html',{
    'list_of_datasets':list_of_datasets,
    'username': current_user.username
    })

@login_required(login_url='login')
def browse_dataset(request):
    if request.method == 'POST':
        data = process_csv.fetch_csv_header(CSV_FILE_BASE_PATH + request.POST.get('id'))

        load_df = pd.read_csv(CSV_FILE_BASE_PATH + request.POST.get('id'))
        # Size of dataset
        dataset_size = len(load_df)

        # Two categories of datasets algorithm (Small - Huge)
        small_datasets_algorithms = ['Stochastic Gradient Descent Regressor', 'Simple Linear Regression', 'Multiple Linear Regression']
        huge_datasets_algorithms = ['Lasso Regressor', 'ElasticNet', 'Ridge', "Support Vector Regressor(kernel = 'Linear')", "Support Vector Regressor(kernel = 'rbf')"]

        response_data = [None]*3
        response_data[0] = data
        response_data[1] = request.POST.get('id')
        # In case dataset size is less than 100K load small_datasets_algorithms else load huge_datasets_algorithms
        response_data[2] = small_datasets_algorithms if dataset_size < 100000 else huge_datasets_algorithms

        return JsonResponse(response_data, content_type="application/json", safe=False)
    else:
        return render(request,'home.html')

@login_required(login_url='login')
def train_classification_model(request):
    if request.method == 'POST':

        selected_algorithm = request.POST.get('typeOfAlgorithm')
        dataset_name = request.POST.get('dataset-name')
        independent_variables = request.POST.getlist('independentVariables')
        dependent_variable = request.POST.get('dependentVariable')

        if selected_algorithm == 'Logistic Regression':
            passed_data = logistic_regression_API.trainer(independent_variables, dependent_variable, dataset_name)
        elif selected_algorithm == "Stochastic Gradient Descent Classifier":
            passed_data = SGD_Classifier_API.trainer(independent_variables, dependent_variable, dataset_name)
        elif selected_algorithm == "Kernel Approximation (RBFSampler)":
            passed_data = RBF_Sampler_API.trainer(independent_variables, dependent_variable, dataset_name)
        elif selected_algorithm == "Kernel Approximation (Nystroem)":
            passed_data = Nystroem_API.trainer(independent_variables, dependent_variable, dataset_name)
        else:
         return HttpResponse("No Algorithm selected")

        return render(request, 'trained-model.html',{
                'score':round(passed_data[0],2),
                'model_attr': passed_data[1],
                'msg': "Model trained successfully !"
                })
    else:
        return render(request,'home.html')

@login_required(login_url='login')
def analysis_page(request):
    # get_model = TrainedModels.objects.get(id=24)
    # list_of_model_attr = get_model.trained_model_attr.split(',')
    return render(request, 'analysis-page.html')

@login_required(login_url='login')
def make_new_prediction(request):
    if request.method == 'POST':
        selected_problem = [None]*1
        convert_list = list(request.POST.items())
        convert_list.pop(0)
        for k, v in convert_list:
            if(k == 'selected-algorithm'):
                selected_problem[0] =  v
        if(selected_problem[0] == "Logistic Regression"):
            predicted = predictor_API.get_prediction(convert_list)
            return JsonResponse(predicted, content_type="application/json", safe=False)
        elif(selected_problem[0] == "Simple Linear Regression"):
            predicted = predictor_API.get_regression_value(convert_list)
            return JsonResponse(predicted, content_type="application/json", safe=False)
        elif(selected_problem[0] == "Multiple Linear Regression"):

            predicted = predictor_API.get_multiple_regression_value(convert_list)
            return JsonResponse(predicted, content_type="application/json", safe=False)
        elif(selected_problem[0] == "Stochastic Gradient Descent Regressor"):

            predicted = predictor_API.get_sgd_regressor_value(convert_list)
            return JsonResponse(predicted, content_type="application/json", safe=False)
        elif(selected_problem[0] == "Stochastic Gradient Descent Classifier"):
            predicted = predictor_API.get_prediction(convert_list)
            return JsonResponse(predicted, content_type="application/json", safe=False)
        elif(selected_problem[0] == "Kernel Approximation (RBFSampler)"):
            predicted = predictor_API.get_prediction(convert_list)
            return JsonResponse(predicted, content_type="application/json", safe=False)
        elif(selected_problem[0] == "Kernel Approximation (Nystroem)"):
            predicted = predictor_API.get_prediction(convert_list)
            return JsonResponse(predicted, content_type="application/json", safe=False)
        else:
            return HttpResponse("No algorithm found !")

@login_required(login_url='login')
def train_clustering_model(request):
    if request.method == 'POST':
        #Determine which model to call
        selected_algorithm = request.POST.get('typeOfAlgorithm')

        if selected_algorithm == 'K-Means':
            passed_data = k_means.trainer()

            return render(request, 'k-mean-clustering-result.html', {
            'shape':passed_data[0],
            'describe':passed_data[1],
            'uri':passed_data[2],
            'head':passed_data[3]
            })
        elif selected_algorithm == "Mean-shift":
            return HttpResponse(selected_algorithm)
        else:
         return HttpResponse("No Algorithm selected")
    else:
        return render(request,'home.html')

@login_required(login_url='login')
def models_page(request):
    current_user = request.user
    list_of_models = TrainedModels.objects.filter(user_id=current_user.id).order_by('id')[::-1]
    return render(request, 'models.html',{
    'list_of_models':list_of_models,
    'username':current_user.username
    })

@login_required(login_url='login')
def open_model(request, id):
    get_model = TrainedModels.objects.get(user_id=1, id=id)
    model_name = get_model.name
    model_columns = get_model.model_columns
    list_of_model_attr = get_model.trained_model_attr.split(',')
    selected_algorithm = get_model.selected_algorithm

    warning_msg = ""
    if selected_algorithm == "Kernel Approximation (RBFSampler)":
        warning_msg = "You have " + str(len(list_of_model_attr)) + " " + "attributes, " + " " + " but this model expects 100 features."

    return render(request, 'analysis.html', {
    'list_of_model_attr':list_of_model_attr,
    'model_name': model_name,
    'model_columns': model_columns,
    'selected_algorithm':selected_algorithm,
    'warning_msg': warning_msg
    })

@login_required(login_url='login')
def train_regression_model(request):
    if request.method == 'POST':

        selected_algorithm = request.POST.get('typeOfAlgorithm')

        dataset_name_reg = request.POST.get('dataset-namereg')
        independent_variables_reg = request.POST.getlist('independentVariablesReg')
        dependent_variable_reg = request.POST.get('dependentVariableReg')

        if selected_algorithm == "Multiple Linear Regression":

            passed_data = multiple_linear_regression_API.trainer(dataset_name_reg,dependent_variable_reg, independent_variables_reg)

            return render(request, 'regression-result.html', {
            'score':round(passed_data[0],2),
            'model_attr': passed_data[1],
            'msg': "Model trained successfully !"
            })
        elif selected_algorithm == "Simple Linear Regression":

            passed_data = simple_linear_regression_API.trainer(dataset_name_reg,dependent_variable_reg, independent_variables_reg )
            return render(request, 'regression-result.html', {
            'score':round(passed_data[0],2),
            'model_attr': passed_data[1],
            'msg': "Model trained successfully !"
            })

        elif selected_algorithm == "Stochastic Gradient Descent Regressor":
            passed_data = SGD_Regressor_API.trainer(dataset_name_reg,dependent_variable_reg, independent_variables_reg )
            return render(request, 'regression-result.html', {
            'score':round(passed_data[0],2),
            'model_attr': passed_data[1],
            'msg': "Model trained successfully !"
            })
        else:
         return HttpResponse("No Algorithm selected")
    else:
        return render(request,'home.html')

@login_required(login_url='login')
def browse_clf_dataset(request):
    if request.method == 'POST':
        CSV_FILE_BASE_PATH="C:\\Users\\23058\\Documents\\GCU\\Honours Project\\artifact\\hpwebapp\\uploads\\"
        data = process_csv.fetch_csv_header(CSV_FILE_BASE_PATH + request.POST.get('id'))

        load_df = pd.read_csv(CSV_FILE_BASE_PATH + request.POST.get('id'))
        dataset_set = len(load_df)

        print("----------------Dataset----------------------------------")

        small_datasets_algorithms = ['Stochastic Gradient Descent Classifier', 'Logistic Regression', 'Kernel Approximation (RBFSampler)', 'Kernel Approximation (Nystroem)']
        huge_datasets_algorithms = ['Linear SVC', 'K Neighbors Classifier', 'Gaussian Naive Bayes', "Support Vector Classifier", "Bagging Classifier", "Random Forest Classifier"]

        response_data = [None]*3
        response_data[0] = data
        response_data[1] = request.POST.get('id')
        response_data[2] = small_datasets_algorithms if dataset_set < 100000 else huge_datasets_algorithms

        return JsonResponse(response_data, content_type="application/json", safe=False)
    else:
        return render(request,'home.html')

@login_required(login_url='login')
def analysis_chart(request):
    labels = []
    data = []

    queryset = TrainedModels.objects.order_by('-accuracy_score')[:10]

    for entry in queryset:
        labels.append(entry.selected_algorithm)
        data.append(entry.accuracy_score)


    return JsonResponse(data={
        'labels': labels,
        'data': data,
    })

@login_required(login_url='login')
def user_guide_page(request):
        current_user = request.user
        list_of_datasets = UserDatasets.objects.filter(user_id=current_user.id).order_by('id')[::-1]
        return render(request, 'user-guide.html',{
        'list_of_datasets':list_of_datasets,
        'username': current_user.username
        })


############################# Authentication ################################################
def register_page(request):
	if request.user.is_authenticated:
		return redirect('home')
	else:
		form = CreateUserForm()
		if request.method == 'POST':
			form = CreateUserForm(request.POST)
			if form.is_valid():
				form.save()
				user = form.cleaned_data.get('username')
				messages.success(request, 'Account was created for ' + user)
				return redirect('login')
		context = {'form':form}
		return render(request, 'register.html', context)

def login_page(request):
	if request.user.is_authenticated:
		return redirect('home')
	else:
		if request.method == 'POST':
			username = request.POST.get('username')
			password =request.POST.get('password')

			user = authenticate(request, username=username, password=password)

			if user is not None:
				login(request, user)
				return redirect('home')
			else:
				messages.info(request, 'Username OR password is incorrect')

		context = {}
		return render(request, 'login.html', context)

def logout_user(request):
	logout(request)
	return redirect('login')
##############################End Auth       ###############################################
