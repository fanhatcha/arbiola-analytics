# Import dependencies
import pandas as pd
import numpy as np

import joblib
from arbiola.models import TrainedModels
import uuid

from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline

from sklearn.kernel_approximation import RBFSampler


def trainer(independent_variables,dependent_variable_passed, dataset_name):

    DATASET_BASE_PATH = "C:\\Users\\23058\\Documents\\GCU\\Honours Project\\artifact\\hpwebapp\\uploads\\"
    MODEL_BASE_PATH='C:/Users/23058/Documents/GCU/Honours Project/artifact/hpwebapp/uploads'
    df = pd.read_csv(DATASET_BASE_PATH + dataset_name)

    summary = [None]*4

    include = independent_variables
    include.append(dependent_variable_passed)
    df_ = df[include]

    categoricals = []
    for col, col_type in df_.dtypes.iteritems():
         if col_type == 'O':
              categoricals.append(col)
         else:
              df_[col].fillna(0, inplace=True)

    df_ohe = pd.get_dummies(df_, columns=categoricals, dummy_na=True)

    dependent_variable = dependent_variable_passed
    x = df_ohe[df_ohe.columns.difference([dependent_variable])]
    y = df_ohe[dependent_variable]


    rbf_feature = RBFSampler(gamma=1, random_state=1, n_components=len(x.columns))
    X_features = rbf_feature.fit_transform(x)
    # clf = SGDClassifier(max_iter=5)
    clf = make_pipeline(StandardScaler(),SGDClassifier(max_iter=1000, tol=1e-3))

    clf.fit(X_features, y)

    # Save your model
    joblib_file = str(uuid.uuid4()) + ".pkl"
    path = MODEL_BASE_PATH +'/'+joblib_file
    joblib.dump(clf, path)

    del independent_variables[-1]
    trained_model_attr = ','.join(independent_variables)

    # Load the model that you just saved
    clf = joblib.load(path)
    # Calculate the Score

    score = clf.score(X_features, y)
    # Print the Score
    print("Test score: {0:.2f} %".format(100 * score))

    # Saving the data columns from training
    model_column_name = str(uuid.uuid4()) + "_columns.pkl"
    persistent_path = MODEL_BASE_PATH +'/'+ model_column_name
    model_columns = list(x.columns)
    joblib.dump(model_columns, persistent_path)

    trained_models = TrainedModels.objects.create(name=joblib_file, trained_model_attr=trained_model_attr, model_columns=model_column_name, user_id=1, accuracy_score=round(100 * score, 2), selected_algorithm="Kernel Approximation (RBFSampler)")
    trained_models.save()

    summary[0] = score*100
    summary[1] = trained_model_attr

    return summary
