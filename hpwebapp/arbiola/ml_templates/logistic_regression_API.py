# Import dependencies
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression

import joblib
from arbiola.models import TrainedModels
import uuid
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix,classification_report
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split

DATASET_BASE_PATH = "C:\\Users\\23058\\Documents\\GCU\\Honours Project\\artifact\\hpwebapp\\uploads\\"
MODEL_BASE_PATH='C:/Users/23058/Documents/GCU/Honours Project/artifact/hpwebapp/uploads'

def trainer(independent_variables,dependent_variable_passed, dataset_name):
    df = pd.read_csv(DATASET_BASE_PATH + dataset_name)

    summary = [None]*4

    include = independent_variables
    include.append(dependent_variable_passed)
    df_ = df[include]

    categoricals = []
    for col, col_type in df_.dtypes.iteritems():
         if col_type == 'O':
              categoricals.append(col)
         else:
              df_[col].fillna(0, inplace=True)

    df_ohe = pd.get_dummies(df_, columns=categoricals, dummy_na=True)

    dependent_variable = dependent_variable_passed
    X = df_ohe[df_ohe.columns.difference([dependent_variable])]
    y = df_ohe[dependent_variable]
    X_train,X_test,y_train,y_test= train_test_split(X,y, test_size=0.2, random_state=45)

    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)

    lr = make_pipeline(sc,LogisticRegression())
    lr.fit(X_train, y_train)
    y_pred = lr.predict(X_test)
    

    # Save your model
    joblib_file = str(uuid.uuid4()) + ".pkl"
    path = MODEL_BASE_PATH +'/'+joblib_file
    joblib.dump(lr, path)

    del independent_variables[-1]
    trained_model_attr = ','.join(independent_variables)

    # Load the model that you just saved
    lr = joblib.load(path)

    # Score is Mean Accuracy
    score = lr.score(X_test, y_test)

    cm = confusion_matrix(y_test, y_pred)
    print(cm)
    # Print the Score
    print("Test score: {0:.2f} %".format(100 * score))
    cl_report=classification_report(y_test,y_pred)
    print(cl_report)
    # Saving the data columns from training
    model_column_name = str(uuid.uuid4()) + "_columns.pkl"
    persistent_path = MODEL_BASE_PATH +'/'+ model_column_name
    model_columns = list(X.columns)
    joblib.dump(model_columns, persistent_path)

    trained_models = TrainedModels.objects.create(name=joblib_file, trained_model_attr=trained_model_attr,
                             model_columns=model_column_name, user_id=1, accuracy_score=round(100 * score, 2),
                             selected_algorithm="Logistic Regression")
    trained_models.save()

    summary[0] = score*100
    summary[1] = trained_model_attr

    return summary
