# Stochastic Gradient Descent

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from sklearn.model_selection import train_test_split

import io
import urllib, base64

#Calculating the R squared value
from sklearn.metrics import r2_score

from sklearn.model_selection import train_test_split

import uuid
import joblib
from arbiola.models import TrainedModels

from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.linear_model import SGDRegressor


def trainer(dataset_name, y_passed, X_passed):

    DATASET_BASE_PATH = "C:\\Users\\23058\\Documents\\GCU\\Honours Project\\artifact\\hpwebapp\\uploads\\"
    MODEL_BASE_PATH='C:/Users/23058/Documents/GCU/Honours Project/artifact/hpwebapp/uploads'
    data = pd.read_csv(DATASET_BASE_PATH + dataset_name)

    result_values = [None]*4
    
    #Initializing the variables
    X = data.drop([str(y_passed)], axis=1)
    y = data[str(y_passed)].values.reshape(-1,1)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)


    reg = make_pipeline(StandardScaler(),SGDRegressor(max_iter=1000, tol=1e-3))
    reg.fit(X_train, y_train)


    #predicting the Test set result
    y_pred = reg.predict(X_test)

    # Save RL_Model to file in the current working directory
    print("---------------Saving into db-------------------------------------")

    # Save your model
    joblib_file = str(uuid.uuid4()) + ".pkl"
    path = MODEL_BASE_PATH +'/'+joblib_file
    joblib.dump(reg, path)

    trained_model_attr = ','.join(X_passed)

    # Load the model that you just saved
    lr = joblib.load(path)
    # Calculate the Score
    score = lr.score(X, y)
    # Print the Score
    print("Test score: {0:.2f} %".format(100 * score))

    trained_models = TrainedModels.objects.create(name=joblib_file, trained_model_attr=trained_model_attr, model_columns=0, user_id=1, accuracy_score=round(100 * score, 2), selected_algorithm="Stochastic Gradient Descent Regressor")
    trained_models.save()

    result_values[0] = score*100
    result_values[1] = trained_model_attr

    print("---------------Saving into db-------------------------------------")

    return result_values
