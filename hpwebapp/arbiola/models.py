from django.db import models

class TrainedModels(models.Model):
    name = models.CharField(max_length=100)
    trained_model_attr = models.TextField()
    model_columns = models.TextField()
    user_id = models.IntegerField()
    accuracy_score = models.CharField(max_length=100)
    selected_algorithm = models.CharField(max_length=100)


    class Meta:
        db_table = "trained_models"

class UserDatasets(models.Model):
    dataset_name = models.CharField(max_length=100)
    saved_dataset_name = models.TextField()
    user_id = models.IntegerField()

    class Meta:
        db_table = "user_datasets"
