from django.shortcuts import render
from django.http import HttpResponse

def supervised_learning_page(request):
    return render(request, 'supervised-learning.html')
