import pickle

import joblib
import numpy as np #numerical python library
import pandas as pd
import traceback
import json

BASE_PATH ='C:/Users/23058/Documents/GCU/Honours Project/artifact/hpwebapp/uploads'

def get_prediction(passed_attributes):

    model_variances = [None]*2

    for k, v in passed_attributes:
        if(k == 'model-name'):
            model_variances[0] =  v
        if(k == 'model-columns'):
             model_variances[1] =  v

    joblib_file = model_variances[0]
    print(joblib_file)
    joblib_file_columns = model_variances[1]
    print(joblib_file_columns)
    PATH_MODEL = BASE_PATH+'/'+joblib_file
    PATH_MODEL_COLUMNS =  BASE_PATH+'/'+joblib_file_columns

    lr = joblib.load(PATH_MODEL) # Load "model.pkl"
    print ('Model loaded')
    model_columns = joblib.load(PATH_MODEL_COLUMNS) # Load "model_columns.pkl"
    print ('Model columns loaded')

    result = {}
    print("**---*---")
    print(passed_attributes)
    print("**---*---")
    for k, v in passed_attributes:
        result[k] = v
    json_data = json.dumps(result)
    # print(json_data)
    result_2=[]
    result_2.append(eval(json_data))
    # print(result_2)
    query = pd.get_dummies(pd.DataFrame(result_2))
    query = query.reindex(columns=model_columns, fill_value=0)
    print("-----------------------------------------------------------------------------------------------------")
    print(model_columns)
    print("-----------------------------------------------------------------------------------------------------")
    print(query)
    print("-----------------------------------------------------------------------------------------------------")
    prediction = lr.predict(query)
    return str(prediction)



def get_regression_value(passed_attributes):

    model_variances = [None]*1

    for k, v in passed_attributes:
        if(k == 'model-name'):
            model_variances[0] =  v

    joblib_file = model_variances[0]

    PATH_MODEL = BASE_PATH+'/'+joblib_file

    print("--------------loading model-------------------------------------")
    loaded_model = joblib.load(PATH_MODEL)

    print ('Model loaded')
    print("--------------loading model-------------------------------------")

    result = {}
    for k, v in passed_attributes:
        if(k == "model-name" or k == "model-columns" or k == "selected-algorithm"):
            print("escaped")
        else:
            result[k] = v

    input = 0
    for x, y in result.items():
        input = y

    print("Result is being loaded !")
    # print(type(input))
    prediction = loaded_model.predict([[float(input)]])
    return str(prediction[0][0])


def get_multiple_regression_value(passed_attributes):

    model_variances = [None]*1

    for k, v in passed_attributes:
        if(k == 'model-name'):
            model_variances[0] =  v

    joblib_file = model_variances[0]

    PATH_MODEL = BASE_PATH+'/'+joblib_file

    print("--------------loading model-------------------------------------")
    loaded_model = joblib.load(PATH_MODEL)
    print(loaded_model)
    print ('Model loaded')
    print("--------------loading model-------------------------------------")

    result = {}
    for k, v in passed_attributes:
        if(k == "model-name" or k == "model-columns" or k == "selected-algorithm"):
            print("escaped")
        else:
            result[k] = v

    length = len(result.items())
    input = []

    print(result.items())
    print(input)

    for x, y in result.items():
        input.append(float(y))

    print("Result is being loaded !")
    print(input)
    for i in input:
        print(i)
    prediction = loaded_model.predict([input])
    return str(prediction[0][0])


def get_sgd_regressor_value(passed_attributes):

    model_variances = [None]*1

    for k, v in passed_attributes:
        if(k == 'model-name'):
            model_variances[0] =  v

    joblib_file = model_variances[0]

    PATH_MODEL = BASE_PATH+'/'+joblib_file

    print("--------------loading model-------------------------------------")
    loaded_model = joblib.load(PATH_MODEL)
    print(loaded_model)
    print ('Model loaded...')
    print("--------------loading model-------------------------------------")

    result = {}
    for k, v in passed_attributes:
        if(k == "model-name" or k == "model-columns" or k == "selected-algorithm"):
            print("escaped")
        else:
            result[k] = v

    length = len(result.items())
    input = []

    print(result.items())
    print(input)

    for x, y in result.items():
        input.append(float(y))

    print("Result is being loaded !")
    print(input)
    for i in input:
        print(i)
    prediction = loaded_model.predict([input])
    return str(prediction[0])
