import csv

def fetch_csv_header(str_path):
    with open(str_path, "r") as f:
        reader = csv.reader(f)
        i = next(reader)
        rest = [row for row in reader]
        print(i)
    return i
